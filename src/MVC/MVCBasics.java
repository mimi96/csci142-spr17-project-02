package MVC;

import MVC.PokerController;
import MVC.MVCBasics;

public class MVCBasics
{
    // Properties
    private PokerController myController;
    
    // Methods
    public static void main(String[] args)
    {
        new MVCBasics();
    }
    
    public MVCBasics()
    {
        setController(new PokerController());
    }

	public void setController(PokerController controller) 
	{
		myController = controller;
	}

	public PokerController getController() 
	{
		return myController;
	}
}