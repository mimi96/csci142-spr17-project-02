package MVC;

import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import model.Card;
import model.ComputerPlayer;
import model.Player;
import model.PokerHandRanking;
import model.PokerModel;

/**
 * controller for game
 * 
 * @author Mimi
 *
 */

public class PokerController 
{
	/**
	 * Instantiating all of my variables I will need for this class
	 */
	public PokerModel myModel;
	public PokerView myView;
	
	private ComputerPlayer myComputerPlayer;
	private PokerHandRanking myHandRanking;
	
	private Player myWins,
				   myLoses;
	
	public boolean start= false,
				   drEckroth = false; 
	
	
////	/*
//	 * my main method to start my game and to show it
//	 */
//	public static void main (String[] args)
//	{
//		
//		new PokerController();
//	}
//	
	/**
	 * Constructor for Poker game functionality setup
	 */
	public PokerController()
	{
		Player myPlayer = new Player (askPlayerName());
		myModel = new PokerModel(myPlayer);
		myComputerPlayer = (ComputerPlayer) myModel.getPlayer(1);
		
		myModel.dealCards();
		myModel.getPlayer(0).getHand().orderCards();
		myModel.getPlayer(1).getHand().orderCards();
		
		myView = new PokerView(this);
	}
	
	public PokerController (PokerView pView)
	{
		Player playerView = new Player (askPlayerName());
		myModel = new PokerModel(playerView);
		myComputerPlayer = (ComputerPlayer) myModel.getPlayer(1);
		myModel.dealCards();
		myModel.getPlayer(0).getHand().orderCards();
		myModel.getPlayer(1).getHand().orderCards();
		myView = pView;
		
	}
	
	
	
	/**
	 * Start method for the game 
	 * @return
	 */
	
	/**
	 * The start method. For this method I will have to begin
	 *  with having all cards not showing before I begin the game.
	 *  Then only my cards will show and Dr.Eckroth's cards will be hidden 
	 *  from me.
	 */
	public void startGame()
	{
		int hand = 5;
		
		myModel.dealCards();
			
		myModel.getPlayer(0).getHand().orderCards();
		myModel.getPlayer(1).getHand().orderCards();
			
		/**
		 * if they are facing up which they are not 
		 * in the beginning of the game
		 */
		boolean EckrothCards = false; 
		
		/**
		 * A for loop to go through all of Dr. Eckroth's cards 
		 * and check if they are faced up or down to begin the game
		 */
		for(int i = 0; i < hand; i ++ )
		{
			if (myModel.getPlayer(1).getHand().
					getCards().get(i).isFaceUp())
			{
				EckrothCards = EckrothCards && true;
			}
		}
		
		if (EckrothCards)
		{
			myView.revealEckrothCards();
		}
		
		myView.revealPlayerCards();
		start = true;	
	}
	
	/**
	 *  The toggle method counts how many cards have been selected just before discarding 
	 *  Needs more work
	 * 
	 */
	
	public void toggle(Integer toggle)
	{
		int myIndex = toggle.intValue();
		int count=0;
		if(start)
		{
			for(int i=0;i<5;i++)
			{
				if(myModel.getPlayer(0).getHand().getCards().get(i).isSelected()==true)
				{
					count++;
				}
			}
			if(count>=3)
			{
				if(myModel.getPlayer(0).getHand().getCards().get(myIndex).isSelected()==false)
				{
					
				}
				else
				{
					myModel.getPlayer(0).getHand().getCards().get(myIndex).toggleSelected();
					myView.selectCards(myIndex,myModel.getPlayer(0).getHand().getCards().get(myIndex).isSelected());
				}
			}
			else
			{
				myModel.getPlayer(0).getHand().getCards().get(myIndex).toggleSelected();
				myView.selectCards(myIndex,myModel.getPlayer(0).getHand().getCards().get(myIndex).isSelected());
			}
		}
	}
	
	/**
	 * The discard method 
	 */
		
	public void discard()
	{
		drEckroth=false;
		Vector<Card> discards = myModel.getPlayer(0).getHand().discard();
		
		myModel.dealCards();
		myModel.getPlayer(0).getHand().orderCards();
		myHandRanking = myModel.getPlayer(0).getHand().determineRanking();
		
		myView.revealPlayerCards();
		myModel.switchTurns();
		other();
		changeTurn();
	}
	
	
	
	public void endYourTurn()
	{
		myWins = myModel.determineWinner();
		
		if (myWins == myModel.getPlayer(0))
		{
			myLoses = myModel.getPlayer(1);
		}
		else
		{
			myLoses = myModel.getPlayer(0);
		}
		
		myView.revealEckrothCards();
		myView.reveal(myView.drEckrothName, myModel.getPlayer(0).getName() 
		+ " " + myModel.getPlayer(0).getNumberWins());	
	}
	
	
		
		
	
	
	
	public void changeTurn()
	{
		myWins = myModel.determineWinner();
		if(myWins == myModel.getPlayer(0))
		{
			myLoses = myModel.getPlayer(1);
		}
		else
		{
			myLoses = myModel.getPlayer(0);
		}
		myView.revealEckrothCards();;
		
		
		if(myModel.getPlayer(0).getNumberWins()==3)
		{
			theEnd(0);	
		}
		else if(myModel.getPlayer(1).getNumberWins()==3)
		{
			
			theEnd(1);
		}
		else
		{
			anotherTurn();
		}
	}
	
	
	public void anotherTurn()
	{
			int reply = JOptionPane.showConfirmDialog(null,  myWins.getName()+" won with a "+ myWins.getHand().determineRanking().name()+"\n"+ myLoses.getName()+" had a "+ myLoses.getHand().determineRanking().name()+". \nAnother Round?", "Play Again?", JOptionPane.YES_NO_OPTION);
	       
			if (reply == JOptionPane.YES_OPTION) 
	        {
	        	myView.revealEckrothCards();
	    		myModel.resetGame();
	    		drEckroth=false;
	    		start=false;
	    		startGame();
	    		
	        }
	        else 
	        {
	           JOptionPane.showMessageDialog(null, "over","",JOptionPane.INFORMATION_MESSAGE);
	           System.exit(0);
	        }
	}
	
	
	public void other()
	{
		drEckroth=true;
		Vector<Integer> myCompDiscards = myComputerPlayer.selectCardsToDiscard();
		myModel.getPlayer(1).getHand().discard(myCompDiscards);
		myModel.dealCards();
		myModel.getPlayer(1).getHand().orderCards();
	}
	
	
	
	
	
	public void startOrDiscard()
	{
		if(start)
		{
			discard();
			myHandRanking = myModel.getPlayer(0).getHand().determineRanking();
			myView.reveal(myView.handRank, "Your Hand is: "+myHandRanking.name());
		}
		else
		{
			startGame();
			myHandRanking = myModel.getPlayer(0).getHand().determineRanking();
			myView.reveal(myView.handRank, "Your Hand is: "+myHandRanking.name());
		}
	}
	
	
	/**
	 * Get the images from the PokerModel class 
	 * Need to use this to set up my images on screen
	 */
	
	public ImageIcon cardImages(int me, int i)
	{
		ImageIcon imgs = new ImageIcon (myModel.getPlayer(me).getHand().getCards().get(i).getImage());
		
		return imgs;
	}

	
	
	
	public String getName (int name)
	{
		return myModel.getPlayer(name).getName();
	}
	
	public String win (int win)
	{
		String myWins; 
		
		myWins = "" + myModel.getPlayer(win).getNumberWins();
		
		return myWins;
	}
	
	
	public static String askPlayerName()
	{
		String name = JOptionPane.showInputDialog(null,"name", JOptionPane.INFORMATION_MESSAGE);
		return name; 
	}
	
	public void theEnd (int end)
	{
		JOptionPane.showMessageDialog(null, myModel.getPlayer(end).getName() + " won", "Congratulations", JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}
	
	public static String askInfo()
	{
		String importantInformation = JOptionPane.showInputDialog(null,"info for end of game", JOptionPane.INFORMATION_MESSAGE);
		return importantInformation; 
	}
	
	
	

}
