package MVC;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.lang.reflect.Method;
import java.util.Vector;

import javax.smartcardio.Card;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import java.awt.event.MouseListener;

import com.sun.prism.Image;

import MVC.ButtonListener;

public class PokerView extends JFrame
{
	/**
	 * Instantiating all of my variables I will need for this class
	 */
	public final static int cardsInHand = 5;
	
	private PokerController myController;
	
	public JButton start,
				   discard;
	
	private ButtonListener[] pokerButtonListener;
	private ButtonListener   buttonListener;
	
	public int width = 1500,
			   height = 1000,
			   score = 0; 
	
	public JLabel[] myCards, otherCards;
	
	public JLabel myPlayerName,
				  drEckrothName,
				  myPlayerScore,
				  drEckrothScore,
				  infoMessage,
				  handRank;
	
	public JPanel gamePanel, 
				  infoPanel,
				  buttonPanel,
				  gameMessagePanel,
				  myPlayerPanel,
				  otherPlayerPanel; 
	
	
	
	public PokerView (PokerController controller)
	{
		
	
		 
		/**
		 * Main frame for the game
		 */
		JFrame frame = new JFrame("Poker Game - Mimi");
		frame.setResizable(false);
		frame.setSize(1500, 1000);
		frame.setLayout(new GridLayout(5,1));
		frame.setLocationRelativeTo(null);
		
		
		/**
		 * Panels for Players 
		 */
		JPanel playerPanel = new JPanel();
		playerPanel.setLayout(new GridLayout(1,5));
		
		
		JPanel eckrothPanel = new JPanel();
		eckrothPanel.setLayout(new GridLayout(1,5));
	
		
		gamePanel = new JPanel();
		
		
		/**
		 * Make new arrays of cards with 5 cards in a deck for both players 
		 */
		myCards = new JLabel[5];
		otherCards = new JLabel[5];
	
		
		/**
		 *  Make new JLabels for the cards of the player
		 *  and the computer player...then later on put images in the JLabels.
		 */
		

		
		for (int i = 0 ; i < 5; i++)
		{
	
			myCards[i] = new JLabel();
			otherCards[i] = new JLabel();
		
			
			
			myCards[i].setIcon(new ImageIcon());
			
			otherCards[i].setText("aaa");
			
			
			
			playerPanel.add(myCards[i]);
			eckrothPanel.add(otherCards[i]);
		}
	
		
		
		
		
		/**
		 * JLabels for each player and for their scores
		 */
		drEckrothName = new JLabel();
		myPlayerName = new JLabel();
		drEckrothScore = new JLabel();
		myPlayerScore = new JLabel();
		
		myPlayerName.setText("test");
		gamePanel.add(myPlayerName);
		
		start = new JButton("Start");
		discard = new JButton("Discard");
		
		buttonPanel = new JPanel();
		buttonPanel.add(start);
		buttonPanel.add(discard);
		

		
		
		/**
		 * adding to frame 
		 */
		
		frame.add(gamePanel);
		frame.add(playerPanel);
		frame.add(eckrothPanel);
		frame.add(buttonPanel);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	

	
	/**
	 * Classes for revealing or showing cards for myPlayer,
	 *  otherPLayer and sets text passed through a parameter (reveal)
	 * @param buttons
	 * @param text
	 */
	
	public void reveal (JLabel buttons, String text)
	{
		buttons.setText(text);;
	}
	
	
	public void revealEckrothCards()
	{
		for (int i = 0; i < cardsInHand; i ++)
		{
			myController.myModel.getPlayer(1).getHand().getCards().get(i).flip();
			
			otherCards[i].setIcon(myController.cardImages(1,i));
			otherPlayerPanel.add(otherCards[i]);
		}
	}
	
	public void revealPlayerCards()
	{
		for (int i = 0; i < cardsInHand; i ++)
		{
			if(myController.myModel.getPlayer(0).getHand().getCards().get(i).isFaceUp() == false)
			{
				myController.myModel.getPlayer(0).getHand().getCards().get(i).flip();
			}
			
			otherCards[i].setIcon(myController.cardImages(0, i));
			myPlayerPanel.add(myCards[i]);
		}
	}
	
	
	
	
	/**
	 * Decided to instead of highlight them to pull them down if selected
	 * the way you mentioned in the first lecture on the Poker game
	 *
	 */
	public void selectCards(int i, boolean selectToPull)
	{
		if (selectToPull == true)
		{
			myCards[i].setSize(myCards[i].getWidth(), myCards[i].getHeight() + 20);
		}
		
		else
		{
			myCards[i].setSize(myCards[i].getWidth(), myCards[i].getHeight() - 20);
		}
	}
	
	
	
	/**
	 * get and set the titles for the start and discard buttons 
	 * @param str
	 */
	
	public void setStartButton(String str)
	{
		start.setText(str);
	}
	
	public String getStartButton()
	{
		return start.getText();
	}
	
	public void setDiscardButton(String str)
	{
		discard.setText(str);
	}
	
	public String getDiscardButton()
	{
		return discard.getText();
	}
	
	
	
	/**
	 * Linking the listeners for selecting a card 
	 * and the buttons to the methods in the PokerController that
	 * they should activate.
	 * 
	 */
	public void listeners()
	{
		Class<? extends PokerController> controller;
		
		Method selectCards, startGame;
		
		Class<?>[] method;
		
		controller = myController.getClass();
		
		selectCards = null; 
		startGame = null;
		
		method = new Class[1];
        
        try
        {
           method [0] = Class.forName("java.lang.Integer");
        }
        catch(ClassNotFoundException e)
        {
           String error;
           error = e.toString();
           System.out.println(error);
        }
        try
        {
           selectCards = controller.getMethod("selected",method);
           startGame = controller.getMethod("start",(Class<?>[])null);
        }
        catch(NoSuchMethodException e)
        {
           String error;

           error = e.toString();
           System.out.println(error);
        }
	     catch(SecurityException s)
        {
           String error;

           error = s.toString();
           System.out.println(error);
        }
        
        int i;
        Integer[] args;
        buttonListener = new ButtonListener(myController,startGame, null);
        start.addMouseListener(buttonListener);

        for (i=0; i < cardsInHand; i++)
        {
           args = new Integer[1];
           args[0] = new Integer(i);
           pokerButtonListener[i] = new ButtonListener(myController, selectCards, args);
           myCards[i].addMouseListener(pokerButtonListener[i]);
        }
	}
}
